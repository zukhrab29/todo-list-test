import Vue from 'vue'
import App from './App.vue'
import { Button, Modal, Input, Icon, Select } from 'ant-design-vue';
import store from './store.js';
import './styles/main.css'
import 'ant-design-vue/dist/antd.css';

Vue.config.productionTip = false
Vue.component(Button.name, Button);
Vue.component(Modal.name, Modal);
Vue.component(Input.name, Input);
Vue.component(Icon.name, Icon);
Vue.component(Select.name, Select);

Vue.use(Button);
Vue.use(Modal);
Vue.use(Input);
Vue.use(Icon);
Vue.use(Select);

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
