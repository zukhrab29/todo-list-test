import Vue from 'vue'
import Vuex from 'vuex'
import mockTodos from './mocks/mockTodos'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todos: [],
    loading: false,
    statuses: [{ value: 'new', title: 'Новая' }, { value: 'cancel', title: 'Отменена' }, { value: 'done', title: 'Выполнена' }]
  },
  mutations: {
    SET_TODOS(state, todos) {
      state.todos = todos
    },
    ADD_TODO(state, todo) {
      state.todos.push(todo);
    },
    REMOVE_TODO(state, id) {
      state.todos = state.todos.filter(todo => todo.id !== id)
    },
    UPDATE_TODO(state, { id, field, value}) {
      const todo = state.todos.find(todo => todo.id === id);
      if (todo) {
        todo[field] = value
      }
    },
    TOGGLE_LOADING(state, loading) {
      state.loading = loading
    }
  },
  actions: {
    fetchTodos({ commit }) {
      commit("TOGGLE_LOADING", true)
      return new Promise((resolve) => {
        setTimeout(() => {
          commit('SET_TODOS', mockTodos)
          commit('TOGGLE_LOADING', false)
          resolve(true)
        }, 1000)
      })
    },
    addTodo({ commit }, todo) {
      return new Promise((resolve) => {
        setTimeout(() => {
          commit('ADD_TODO', { ...todo, status: "new", created_at: new Date().toISOString, id: Date.now() })
          resolve(true)
        }, 1000)
      })
    },
    updateTodo({ commit }, updatedTodo) {
      return new Promise((resolve) => {
        setTimeout(() => {
          commit('UPDATE_TODO', updatedTodo)
          resolve(true)
        }, 1000)
      })
    },
    removeTodo({ commit }, id) {
      return new Promise((resolve) => {
        setTimeout(() => {
          commit('REMOVE_TODO', id)
          resolve(true)
        }, 1000)
      })
    }
  }
})